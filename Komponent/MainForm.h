#pragma once

#include <windows.h>

namespace Komponent {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}

	private:
		bool ItemChanged = false;

	System::Windows::Forms::TextBox^  SearchItemField;
	System::Windows::Forms::Button^  SearchItemButton;
	System::Windows::Forms::Label^  label1;
	System::Windows::Forms::Label^  label2;
	System::Windows::Forms::TextBox^  ItemName;
	System::Windows::Forms::TextBox^  ItemTags;
	System::Windows::Forms::Label^  label3;
	System::Windows::Forms::TextBox^  ItemPosition;
	System::Windows::Forms::ListBox^  ItemList;
	System::Windows::Forms::Button^  CreateNewButton;
	System::Windows::Forms::Button^  SaveItemButton;
	System::Windows::Forms::TextBox^  ItemDescription;
	System::Windows::Forms::Label^  label4;
	System::Windows::Forms::GroupBox^  groupBox1;

	
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->SearchItemField = (gcnew System::Windows::Forms::TextBox());
			this->SearchItemButton = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->ItemName = (gcnew System::Windows::Forms::TextBox());
			this->ItemTags = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->ItemPosition = (gcnew System::Windows::Forms::TextBox());
			this->ItemList = (gcnew System::Windows::Forms::ListBox());
			this->CreateNewButton = (gcnew System::Windows::Forms::Button());
			this->SaveItemButton = (gcnew System::Windows::Forms::Button());
			this->ItemDescription = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// SearchItemField
			// 
			this->SearchItemField->Location = System::Drawing::Point(12, 12);
			this->SearchItemField->Name = L"SearchItemField";
			this->SearchItemField->Size = System::Drawing::Size(168, 20);
			this->SearchItemField->TabIndex = 0;
			// 
			// SearchItemButton
			// 
			this->SearchItemButton->Location = System::Drawing::Point(186, 12);
			this->SearchItemButton->Name = L"SearchItemButton";
			this->SearchItemButton->Size = System::Drawing::Size(75, 20);
			this->SearchItemButton->TabIndex = 1;
			this->SearchItemButton->Text = L"Search";
			this->SearchItemButton->UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(31, 27);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Name";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(35, 53);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(31, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Tags";
			// 
			// ItemName
			// 
			this->ItemName->Location = System::Drawing::Point(72, 27);
			this->ItemName->Name = L"ItemName";
			this->ItemName->Size = System::Drawing::Size(100, 20);
			this->ItemName->TabIndex = 4;
			this->ItemName->TextChanged += gcnew System::EventHandler(this, &MainForm::ItemName_TextChanged);
			// 
			// ItemTags
			// 
			this->ItemTags->Location = System::Drawing::Point(72, 53);
			this->ItemTags->Name = L"ItemTags";
			this->ItemTags->Size = System::Drawing::Size(100, 20);
			this->ItemTags->TabIndex = 5;
			this->ItemTags->TextChanged += gcnew System::EventHandler(this, &MainForm::ItemTags_TextChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(22, 79);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(44, 13);
			this->label3->TabIndex = 6;
			this->label3->Text = L"Position";
			// 
			// ItemPosition
			// 
			this->ItemPosition->Location = System::Drawing::Point(72, 79);
			this->ItemPosition->Name = L"ItemPosition";
			this->ItemPosition->Size = System::Drawing::Size(100, 20);
			this->ItemPosition->TabIndex = 7;
			this->ItemPosition->TextChanged += gcnew System::EventHandler(this, &MainForm::ItemPosition_TextChanged);
			// 
			// ItemList
			// 
			this->ItemList->FormattingEnabled = true;
			this->ItemList->Location = System::Drawing::Point(12, 39);
			this->ItemList->Name = L"ItemList";
			this->ItemList->Size = System::Drawing::Size(249, 355);
			this->ItemList->TabIndex = 8;
			// 
			// CreateNewButton
			// 
			this->CreateNewButton->Location = System::Drawing::Point(505, 12);
			this->CreateNewButton->Name = L"CreateNewButton";
			this->CreateNewButton->Size = System::Drawing::Size(75, 20);
			this->CreateNewButton->TabIndex = 9;
			this->CreateNewButton->Text = L"Create new";
			this->CreateNewButton->UseVisualStyleBackColor = true;
			this->CreateNewButton->Click += gcnew System::EventHandler(this, &MainForm::CreateNewButton_Click);
			// 
			// SaveItemButton
			// 
			this->SaveItemButton->Location = System::Drawing::Point(232, 324);
			this->SaveItemButton->Name = L"SaveItemButton";
			this->SaveItemButton->Size = System::Drawing::Size(75, 23);
			this->SaveItemButton->TabIndex = 10;
			this->SaveItemButton->Text = L"Save";
			this->SaveItemButton->UseVisualStyleBackColor = true;
			// 
			// ItemDescription
			// 
			this->ItemDescription->Location = System::Drawing::Point(72, 105);
			this->ItemDescription->Multiline = true;
			this->ItemDescription->Name = L"ItemDescription";
			this->ItemDescription->Size = System::Drawing::Size(235, 213);
			this->ItemDescription->TabIndex = 11;
			this->ItemDescription->TextChanged += gcnew System::EventHandler(this, &MainForm::ItemDescription_TextChanged);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 105);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(60, 13);
			this->label4->TabIndex = 12;
			this->label4->Text = L"Description";
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->ItemPosition);
			this->groupBox1->Controls->Add(this->SaveItemButton);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->ItemDescription);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->ItemName);
			this->groupBox1->Controls->Add(this->ItemTags);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Location = System::Drawing::Point(267, 38);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(313, 353);
			this->groupBox1->TabIndex = 13;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Component";
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(592, 403);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->CreateNewButton);
			this->Controls->Add(this->ItemList);
			this->Controls->Add(this->SearchItemButton);
			this->Controls->Add(this->SearchItemField);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"MainForm";
			this->Text = L"Komponent";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		void ClearTextAndResetColor(System::Windows::Forms::TextBox^ box)
		{
			box->Text = "";
			box->BackColor = Color::White;
		}

		void MarkAsChanged(TextBox^ box)
		{
			box->BackColor = Color::LightYellow;
			this->ItemChanged = true;
		}


		private: System::Void CreateNewButton_Click(System::Object^  sender, System::EventArgs^  e) {

			if (this->ItemChanged) {
				System::Windows::Forms::DialogResult result = MessageBox::Show(this, "Item changed. Discard and create new one?", "Item changed", MessageBoxButtons::YesNo);
				if (result == System::Windows::Forms::DialogResult::No)
				{
					return;
				}
			}

			ClearTextAndResetColor(this->ItemName);
			ClearTextAndResetColor(this->ItemTags);
			ClearTextAndResetColor(this->ItemPosition);
			ClearTextAndResetColor(this->ItemDescription);
		}

		private: System::Void ItemName_TextChanged(System::Object^ sender, System::EventArgs^  e) {
			MarkAsChanged(this->ItemName);
		}

		private: System::Void ItemTags_TextChanged(System::Object^ sender, System::EventArgs^  e) {
			MarkAsChanged(this->ItemTags);
		}

		private: System::Void ItemPosition_TextChanged(System::Object^ sender, System::EventArgs^  e) {
			MarkAsChanged(this->ItemPosition);
		}

		private: System::Void ItemDescription_TextChanged(System::Object^ sender, System::EventArgs^  e) {
			MarkAsChanged(this->ItemDescription);
		}
};
}
